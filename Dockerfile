FROM node:10.15.0
RUN mkdir /app
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn && yarn cache clean
COPY . .
RUN yarn jest